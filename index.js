import axios from 'axios';
import {Datastore} from '@google-cloud/datastore'
import express from 'express'
import cors from 'cors'

const app = express();
app.use(express.json())
app.use(cors())

const datastore = new Datastore();

app.get('/messages/:mid', async (req, res) =>{
    const key = datastore.key(['Message',Number(req.params.mid)]);
    const [data,metaInfo] = await datastore.get(key);
    if(!data){
        res.status(404)
        res.send(`The message with ID ${req.params.mid} could not be found.`)
    } else {
        res.status(200)
        res.send(data)
    }
});

app.get('/messages', async (req, res) =>{

    try{
        if(req.query.recipient && req.query.sender){
            const query = datastore.createQuery('Message').filter('recipient','=',req.query.recipient).filter('sender','=',req.query.sender)
            const [data,metaInfo] = await datastore.runQuery(query);
            if(!data[0].recipient || !data[0].sender){
                res.status(404);
                res.send("Sorry, no messages were found.");
            }
            res.status(200);
            res.send(data);
        } else if(req.query.sender){
            const query = datastore.createQuery('Message').filter('sender','=',req.query.sender)
            const [data,metaInfo] = await datastore.runQuery(query);
            if(!data[0].sender){
                res.status(404);
                res.send("Sorry, no messages were found.");
            }
            res.status(200);
            res.send(data);
        } else if(req.query.recipient){
            const query = datastore.createQuery('Message').filter('recipient','=',req.query.recipient)
            const [data,metaInfo] = await datastore.runQuery(query);
            if(!data[0].recipient){
                res.status(404);
                res.send("Sorry, no messages were found.");
            }
            res.status(200);
            res.send(data);
        } else {
            const query = datastore.createQuery('Message');
            const [data,metaInfo] = await datastore.runQuery(query);
            if(!data){
                res.status(404);
                res.send("Sorry, no messages were found.");
            }
            res.status(200);
            res.send(data);
        }
    } catch {
        res.status(404);
        res.send("Sorry, no messages were found.");
    }
})

app.post('/messages', async (req, res) =>{

    try{
        const sender = await axios.get(`https://authorization-dot-wedding-planner-kirk.ue.r.appspot.com/users/${req.body.sender}/verify`)
    } catch {
        res.status(404)
        res.send("The sender address you entered could not be found")
    }
    try{
        const recipient = await axios.get(`https://authorization-dot-wedding-planner-kirk.ue.r.appspot.com/users/${req.body.recipient}/verify`)
    } catch {
        res.status(404)
        res.send("The recipient address you entered could not be found")
    }

    const message = {
        sender: req.body.sender,
        recipient: req.body.recipient,
        note: req.body.note,
        timestamp: Date.now()
    }

    const mid = Math.random() * 999999999;
    const key = datastore.key(['Message',mid])
    const response = await datastore.save({key:key,data:message})
    res.status(201)
    res.send('Message sent successfully')
})
const PORT = process.env.PORT || 3000;
app.listen(PORT, () => console.log(`Application Started on port ${PORT}`));